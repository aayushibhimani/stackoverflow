<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Http\Requests\CreateAnswerRequest;
use App\Http\Requests\UpdateAnswerRequest;
use App\Notifications\NewReplyAdded;
use App\Question;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function store(CreateAnswerRequest $request, Question $question)
    {
        //
        $question->answers()->create([
            'body'=>$request->body,
            'user_id'=>auth()->id()
        ]);

        $question->owner->notify(new NewReplyAdded($question));

        session()->flash('success','Your Answer submitted successfully');
        return redirect($question->url);
    }

    public function show(Answer $answer)
    {
        //
    }

    public function edit(Question $question, Answer $answer)
    {
        //
        $this->authorize('update',$answer);
        return view('answers.edit', compact([
            'question',
            'answer'
        ]));
    }

    public function update(UpdateAnswerRequest $request,Question $question, Answer $answer)
    {
        $answer->update([
            'body'=>$request->body
        ]);
        session()->flash('success','Answer Updated Successfully!');
        return redirect($question->url);
    }

    public function destroy(Question $question,Answer $answer)
    {
        //
        $this->authorize('delete',$answer);
        $answer->delete();

        session()->flash('success','Answer Deleted Successfully!');
        return redirect($question->url);
    }
    public function bestAnswer(Request $request, Answer $answer)
    {
        $this->authorize('markAsBest',$answer);
        $answer->question->markBestAnswer($answer);
        return redirect()->back();
    }
}
